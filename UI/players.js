exports.getPlayersList = getPlayersList;

var _ = require("underscore"),
    players,
    clearPlayersList = [
        {
            'id':'0',            
            'firstName':'Дарт',
            'lastName':'Вейдер',
            'birthYear':'1989',
            'teamId': '0',
            'height':'181',
            'weight': '72'            
        },
        {
            'id':'1',            
            'firstName':'Люк',
            'lastName':'Скайуокер',
            'birthYear':'1990',
            'teamId': '1',
            'height':'180',
            'weight': '70'  
        },
        {
            'id':'2',            
            'firstName':'Оби-Ван',
            'lastName':'Кеноби',
            'birthYear':'1987',
            'teamId': '2',
            'height':'188',
            'weight': '78'  
        },
        {
            'id':'3',            
            'firstName':'Йода',
            'lastName':'Мастер',
            'birthYear':'1989',
            'teamId': '0',
            'height':'179',
            'weight': '71'  
        },
        {
            'id':'4',            
            'firstName':'Хан',
            'lastName':'Соло',
            'birthYear':'1991',
            'teamId': '1',
            'height':'170',
            'weight': '60'  
        },
		{
            'id':'5',            
            'firstName':'Граф',
            'lastName':'Дуку',
            'birthYear':'1989',
            'teamId': '2',
            'height':'181',
            'weight': '72'            
        },
        {
            'id':'6',            
            'firstName':'Дарт',
            'lastName':'Си́диус',
            'birthYear':'1990',
            'teamId': '0',
            'height':'180',
            'weight': '70'  
        },
        {
            'id':'7',            
            'firstName':'Нут',
            'lastName':'Ганрей',
            'birthYear':'1987',
            'teamId': '1',
            'height':'188',
            'weight': '78'  
        },
        {
            'id':'8',            
            'firstName':'Мейс',
            'lastName':'Винду',
            'birthYear':'1989',
            'teamId': '2',
            'height':'179',
            'weight': '71'  
        },
        {
            'id':'9',            
            'firstName':'Квай-Гон',
            'lastName':'Джинн',
            'birthYear':'1991',
            'teamId': '0',
            'height':'170',
            'weight': '60'  
        },
		{
            'id':'10',            
            'firstName':'Дарт',
            'lastName':'Мол',
            'birthYear':'1989',
            'teamId': '1',
            'height':'181',
            'weight': '72'            
        },
        {
            'id':'11',            
            'firstName':'Джа-Джа',
            'lastName':'Бинкс',
            'birthYear':'1990',
            'teamId': '2',
            'height':'180',
            'weight': '70'  
        },
        {
            'id':'12',            
            'firstName':'Чубакка',
            'lastName':'',
            'birthYear':'1987',
            'teamId': '0',
            'height':'188',
            'weight': '78'  
        },
        {
            'id':'13',            
            'firstName':'Асока ',
            'lastName':'Тано',
            'birthYear':'1989',
            'teamId': '1',
            'height':'179',
            'weight': '71'  
        },
        {
            'id':'14',            
            'firstName':'Падме',
            'lastName':'Амидала',
            'birthYear':'1991',
            'teamId': '2',
            'height':'170',
            'weight': '60'  
        }
    ];    


function getPlayersList () {
    return players = _.clone(clearPlayersList);
}