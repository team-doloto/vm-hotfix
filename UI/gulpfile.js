'use strict'

/*
!IMPORTANT!

For build and running tests: gulp build
For debug: gulp debug

*/

var gulp = require('gulp'),
    useref = require('gulp-useref'),
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-minify-css'),
    filter = require('gulp-filter'),
    flatten = require('gulp-flatten'),
    clean = require('gulp-clean'),
    debug = require('gulp-debug'),
    rename = require("gulp-rename"),    
    replace = require('gulp-replace'),
    runSequence = require('run-sequence'),
    qunit = require('gulp-qunit'),
    merge = require('merge-stream'),
    path = '../dotNet/VolleyManagement.UI';

// Build
gulp.task('build', function () {
    runSequence('preBuild', 'replace');
});

gulp.task('preBuild', ['renameForBuild', 'fonts', 'img']);

gulp.task('clean', function () {
    return gulp.src(path, {read: false})
                .pipe(clean({force: true}));
});

gulp.task('ConcatAndMinify', ['clean'], function () {
    var assets = useref.assets();
    
    return gulp.src('client/*.html')
                .pipe(assets)
                .pipe(gulpif('*.js', uglify()))
                .pipe(gulpif('*.css', minifyCss()))
                .pipe(assets.restore())
                .pipe(useref())
                .pipe(gulp.dest(path));   
});

gulp.task('fonts', ['clean'], function () {
    return gulp.src('client/css/fonts/*')
        .pipe(filter('**/*.{eot,svg,ttf,woff}'))
        .pipe(flatten())
        .pipe(gulp.dest(path + '/Content/fonts'));
});

gulp.task('img', ['clean'], function () {
    return gulp.src('client/img/*')
        .pipe(filter('*.jpg'))
        .pipe(flatten())
        .pipe(gulp.dest(path + '/Content/img'));
});

// Rename index.html to _Layout.cshtml
gulp.task('renameForBuild', ['ConcatAndMinify'], function () {
    var renamedIndexStream = gulp.src(path + '/index.html')
        .pipe(rename('/Areas/WebAPI/Views/Shared/_Layout.cshtml'))
        .pipe(gulp.dest(path)),     
    
        removedIndexStream = gulp.src(path + '/index.html', {read: false})
            .pipe(clean({force: true})); 

    return merge(renamedIndexStream, removedIndexStream); //must merge to use run-sequnce plugin (have to return 1 stream per task)
});

// Replace for '~' and anti-Cache
gulp.task('replace', function(){
    var hash = new Date().getTime(),
    
        indexFile = gulp.src([path + '/Areas/WebAPI/Views/Shared/_Layout.cshtml'])
            .pipe(replace('scripts.js', 'scripts.' + hash + '.js'))
            .pipe(replace('styles.css', 'styles.' + hash + '.css'))
            .pipe(replace('href="', 'href="~/'))
            .pipe(replace('src="', 'src="~/'))
            .pipe(replace('</body>', '<div id="body">@RenderBody()</div></body>'))
            .pipe(gulp.dest(path + '/Areas/WebAPI/Views/Shared/')),

        stylesFile =   gulp.src(path + '/Content/styles.css')
            .pipe(rename('/Content/styles.' + hash + '.css'))
            .pipe(gulp.dest(path)),

        initialStylesRemoved = gulp.src(path + '/Content/styles.css', {read: false})
            .pipe(clean({force: true})),

        scriptsFile = gulp.src(path + '/Scripts/scripts.js')
            .pipe(rename('/Scripts/scripts.' + hash + '.js'))
            .pipe(gulp.dest(path)),

        initialScriptsRemoved = gulp.src(path + '/Scripts/scripts.js', {read: false})
            .pipe(clean({force: true}));

    return merge(indexFile, stylesFile, scriptsFile, initialStylesRemoved, initialScriptsRemoved);
});  

// Running tests
gulp.task('test', function() {
    return gulp.src('./tests/index.html')
        .pipe(qunit());
});

// Debug
gulp.task('debug', function () {
    runSequence('preDebug', 'rename', 'finalReplace', 'clean');   
});

gulp.task('preDebug', function () {
    var html = gulp.src('client/*.html')
        .pipe(debug())
        .pipe(gulp.dest(path + '/Areas/WebAPI/Views/Shared')),

        js = gulp.src('client/**/*.js')
        .pipe(debug())
        .pipe(gulp.dest(path + '/scripts')),

        css = gulp.src('client/**/*.css')
        .pipe(debug())
        .pipe(gulp.dest(path + '/Content')),

        fonts = gulp.src('client/**/*.{eot,svg,ttf,woff}')
        .pipe(debug())
        .pipe(gulp.dest(path + '/Content')),

        img = gulp.src('client/**/*.{jpg, gif}')
        .pipe(debug())
        .pipe(gulp.dest(path + '/Content')); 

    return merge(html, js, css, fonts, img);
});

gulp.task('rename', function () {    
    var renameIndexStream = gulp.src(path + '/Areas/WebAPI/Views/Shared/index.html')
        .pipe(rename('/Areas/WebAPI/Views/Shared/_Layout.cshtml'))
        .pipe(gulp.dest(path)),
    
        removeIndexStream = gulp.src(path + '/Areas/WebAPI/Views/Shared/index.html', {read: false})
        .pipe(clean({force: true})); 

    return merge(renameIndexStream, removeIndexStream);
});

gulp.task('finalReplace', function(){
    return gulp.src([path + '/Areas/WebAPI/Views/Shared/_Layout.cshtml'])
        .pipe(replace('href="', 'href="~/Content'))
        .pipe(replace('src="', 'src="~/Scripts'))
        .pipe(gulp.dest(path + '/Areas/WebAPI/Views/Shared/'));
}); 

gulp.task('clean', function () {
    var cleanCss = gulp.src(path + '/Content/*.css', {read: false})
                .pipe(clean({force: true})),

        cleanJs = gulp.src(path + '/Scripts/*.js', {read: false})
                .pipe(clean({force: true}));

    return merge(cleanCss, cleanJs);
});

/*
index: ~/VolleyManagement.UI/Areas/WebAPI/Views/Shared/_Layout.cshtml
CSS: ~/VolleyManagement.UI/Content/
JS: ~/VolleyManagement.UI/Scripts/
*/