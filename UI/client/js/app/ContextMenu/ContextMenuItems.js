"use strict";

(function (This)  {
    This.ContextMenuItems = Backbone.Model.extend({
        defaults: {
            items: '',
            name: ''
        }
    });
})(App.ContextMenu);