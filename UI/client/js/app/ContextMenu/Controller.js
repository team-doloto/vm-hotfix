'use strict';

(function (This) {
    This.Controller = function () {
        var $contextMenu = $('#contextMenu');

        start();

        vm.mediator.subscribe('contextMenuClosed', hideContextMenu);

        function start () {
            setUpMediator();
        }

        function setUpMediator () {
            vm.mediator.subscribe('Context Menu was called', setUpContextMenu);
        }

        function setUpContextMenu (options) {
            var contextMenu = new This.ContextMenuView(options);

            $contextMenu.html(contextMenu.render().$el);
        }

        function hideContextMenu () {
            $contextMenu.hide();
        }

        return this;
    }
})(App.ContextMenu);
