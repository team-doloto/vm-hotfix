"use strict";

(function (This) {
    This.ContextMenuItemsCollection = Backbone.Collection.extend({
        url: '/OData/ContextMenu',

        model: This.ContextMenuItems
    });
})(App.ContextMenu);