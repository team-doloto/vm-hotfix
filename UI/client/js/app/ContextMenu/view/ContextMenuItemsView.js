'use strict';

(function (This) {
    This.ContextMenuItemsView = Backbone.View.extend({
        tagName: 'li',
        className: 'menu-items',

        template: contextMenuItemsTpl,

        initialize: function(modelData) {
            this.modelName = modelData.modelName;
            this.modelId = modelData.id;
            $("html").on("click", function () {
                vm.mediator.publish('contextMenuClosed');
            });
        },

        events: {
            'click' : 'chooseMenuItem'
        },

        render: function (model) {
            this.model = model;

            this.$el.html(this.template({attributes: this.model.attributes}));

            return this;
        },

        chooseMenuItem: function () {
            var itemsId = this.model.get('id'),
                modelName = this.modelName,
                clickHandlers = {
                    '0': this.showInfo,
                    '1': this.edit,
                    '2': this.confirmDel,
                    '3': this.toMainPage
                };

            vm.mediator.publish('contextMenuClosed');

            clickHandlers[itemsId](modelName, this.modelId);
        },

        showInfo: function(modelName ,modelId) {
            vm.mediator.publish('Show' + modelName + 'ById', modelId);
        },

        edit: function (modelName ,modelId) {
            vm.mediator.publish('Edit' + modelName + 'ById', modelId);
        },

        confirmDel: function (modelName ,modelId) {
            vm.mediator.publish('Delete' + modelName + 'ById', modelId);
        },

        toMainPage: function () {
            vm.mediator.publish('MainPageSelected');
        },

        hide: function () {
            $('#contextMenu').hide();
        }
    });
})(App.ContextMenu);