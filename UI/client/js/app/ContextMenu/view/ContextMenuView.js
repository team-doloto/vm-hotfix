'use strict';

(function (This) {
    This.ContextMenuView = Backbone.View.extend({
        tagName: 'ul',
        className: 'list-menu',
        dividerTpl: contextMenuDividerTpl,
        contextMenuItemsList: [
            {
                id: 0,
                icon: 'glyphicon glyphicon-th-list',
                isDivided: false,
                name: 'Показать полную информацию'
            },
            {
                id: 1,
                icon: 'glyphicon glyphicon-pencil',
                isDivided: false,
                name: 'Редактировать'
            },
            {
                id: 2,
                icon: 'glyphicon glyphicon-trash',
                isDivided: true,
                name: 'Удалить'
            },
            {
                id: 3,
                icon: 'glyphicon glyphicon-home',
                isDivided: false,
                name: 'На главную'
            }
        ],

        initialize: function (modelData) {
            this.modelId = modelData.id;
            this.modelName = modelData.nameView;

            this.clientX = modelData.clientX;
            this.clientY = modelData.clientY;

            this.$contextMenu = $('#contextMenu');

            this.$contextMenu.on('click', function (e) { 
                $(this).hide(); 
            });

            this.contextMenuItems = new This.ContextMenuItemsCollection(this.contextMenuItemsList);       
        },

        render: function () {
            this.contextMenuItems.forEach(function (model, i) {
                var contextMenuItem = new This.ContextMenuItemsView({
                    id: this.modelId, 
                    modelName: this.modelName
                    }),
                    x = this.getMenuPosition(this.clientX, 'width', 'scrollLeft'),
                    y = this.getMenuPosition(this.clientY, 'height', 'scrollTop');

                this.$el.append(contextMenuItem.render(model).$el);

                this.$contextMenu
                    .show()
                    .css({left: x, top: y});

                this.divideMenuItems(model);

            }, this);



            return this;
        },

        getMenuPosition: function (mouse, direction, scrollDir) {
            var win = $(window)[direction](),
                scroll = $(window)[scrollDir](),
                menu = this.$contextMenu[direction](),
                position = mouse + scroll;

            if (mouse + menu > win && menu < mouse)
                position -= menu;

            return position;
        },

        divideMenuItems: function (menuItem) {
            var isDivided = menuItem.get('isDivided');

            isDivided && this.$el.append(contextMenuDividerTpl);
        }
    });
})(App.ContextMenu);