'use strict';

(function (This) {
    This.Controller = function () {
        var teams = new This.TeamCollectionView(),
            $teams = $('#main'),
            view;

        start();
        
        function start () {
            setUpMediator();
            $teams.append(teams.render().el);
        }

        function setUpMediator () {
            vm.mediator.subscribe('ShowTeams', showAll);
            vm.mediator.subscribe('ShowTeamById', showViewById);
            vm.mediator.subscribe('EditTeam', editViewById);                        
            vm.mediator.subscribe('CreateTeam', createView);
            vm.mediator.subscribe('DeleteTeamById', deleteViewById);
            vm.mediator.subscribe('TeamViewClosed', viewClosed);
            vm.mediator.subscribe('EditTeamById', editViewById);
            vm.mediator.subscribe('ShowTeamInfo', showView);
        }

        function showAll () {
            hideAll();
            view && view.remove();

            teams.show();
        }

        function showViewById (id) {
            teams.getModelById(id, showView);
        }

        function editViewById (id) {
            teams.getModelById(id, editView);
        }

        function editView (team) {
            hideAll();
            view && view.remove();
            view = new This.CreateEditView({model: team});
            teams.hide();
            $teams.append(view.render().el);
        }

        function createView () {
            hideAll();
            view && view.remove();
            view = new This.CreateEditView();

            teams.hide();
            $teams.append(view.render().el);
        }

        function deleteViewById (id) {
            teams.getModelById(id, deleteView)
        }

        function deleteView (team) {
            view && view.remove();
            view = new This.TeamHomepageView({model: team});
            view.confirmDelete();
        }

        function showView (team) {
            hideAll();
            view && view.remove();
            view = new This.TeamHomepageView({model: team});
            teams.hide();
            $teams.append(view.render().el);
        }

        function viewClosed (id) {
            if(!id) {
                showAll();
            }else{
                teams.getModelById(id, showView);
            }
        }

        function hideAll () {
            $teams.children().addClass('hidden');
        }

        return this;
    }
})(App.Teams);