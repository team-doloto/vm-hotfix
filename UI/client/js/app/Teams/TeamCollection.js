'use strict';

(function (This) {
    This.TeamCollection = Backbone.Collection.extend({
        url: '/OData/Teams',

        model: This.Team
    });
})(App.Teams);