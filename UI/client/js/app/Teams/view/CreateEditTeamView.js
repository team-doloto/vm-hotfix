'use strict';

(function (This) {
    This.CreateEditView = Backbone.View.extend({
        tagName: 'div',

        template: editTeamTpl,

        events: {
            'click .save': 'save',
            'click .cancel': 'cancel',
            'blur input': 'preValidate'
        },

        initialize: function () {
            this.model = this.model || new This.Team();
            this.defaultModelJSON = this.model.toJSON();
            this.modelBinder = new Backbone.ModelBinder();
            
            this.collection =  new App.Players.PlayerCollection();
            this.collection.on('add', this.renderOne, this);
            this.collection.fetch();
            
            Backbone.Validation.bind(this);

            vm.mediator.subscribe('ShowTeamById', this.undoChanges, {}, this);
            vm.mediator.subscribe('captainSelected', this.setCaptain, {}, this);
        },

        render: function () {
            this.$el.append(this.template(this.model.toJSON()));            
            this.modelBinder.bind(this.model, this.el);

            return this;
        },

        save: function () {
            var isNewModel = this.model.isNew();

            if (!this.preValidate()) {
                // Don't showing model in CollectionView if model not saved on server
                this.model.once('sync', function () {
                    if (isNewModel) {
                        vm.mediator.publish('TeamSaved', this.model);
                    }

                    vm.mediator.publish(
                        'Notice',
                        'success',
                        isNewModel? 'Вы успешно зарегистрировали новую команду!': 'Информация успешно изменена!'
                    );

                    vm.mediator.publish(
                        'TeamViewClosed',
                        this.model.id
                    );

                }, this);

                this.model.save();


            }
        },

        preValidate: function (e) {
            var attrName, error, validationResult,
                errors = {};
            if (e) {
                attrName = e.target.name;
                error = this.model.preValidate(
                    attrName, this.model.get(attrName)
                );

                if (error) {
                    vm.mediator.publish('Hint', error, this.$('[name=' + attrName + ']'));
                }

                validationResult = error;
            } else {
                errors = this.model.preValidate({
                    name: this.model.get('name'),
                    captain: this.model.get('captain'),
                    coach: this.model.get('coach'),
                    achievements: this.model.get('achievements')
                });

                if (errors) {
                    for (attrName in errors) {
                        vm.mediator.publish('Hint', errors[attrName], this.$('[name=' + attrName + ']'));
                    }
                }
                validationResult = errors;
            }

            return validationResult;
        },

        cancel: function () {
            this.undoChanges();

            vm.mediator.publish(
                'TeamViewClosed',
                this.model.id
            );
        },

        undoChanges: function () {
            this.modelBinder.unbind();
            this.model.off('change', this.preValidate);
            this.model.set(this.defaultModelJSON);
        },
        
        renderOne: function (person) {
            var playerView = new This.OnePlayerView({model: person}).render();
            $('.cptainSelect').append(playerView.$el);
        }
        
    });
})(App.Teams);