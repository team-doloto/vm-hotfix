'use strict';

(function (This) {
    This.TeamView = Backbone.View.extend({
        tagName: 'li',
        className: 'team list-group-item',

        template: teamTpl,

        events: {
            'click': 'showInfo',
            'contextmenu': 'showContextMenu'
        },

        initialize: function () {
            this.model.on('change', this.render, this);
            this.model.on('remove', this.remove, this);
        },

        showContextMenu: function (e) {
            var options = {
                    id: this.model.get('id'),
                    clientX: e.clientX,
                    clientY: e.clientY,
                    nameView: 'Team'
                };

            $('#contextMenu').attr('teamId', options.id);

            vm.mediator.publish('Context Menu was called', options);

            e.preventDefault();
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));

            return this;
        },

        showInfo: function () {
            vm.mediator.publish('ShowTeamInfo', this.model);
        }
    })
})(App.Teams);