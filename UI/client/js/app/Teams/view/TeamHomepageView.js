"use strict";

(function (This) {
    This.TeamHomepageView = Backbone.View.extend({
        tagName: 'div',
        className: 'homepage',
        name: '',

        template: teamHomepageTpl,

        events: {
            'click .cancel': 'cancel',
            'click .edit': 'edit',
            'click .delete': 'confirmDelete'
        },

        initialize: function (options) {
            this.captainId = options.model.attributes.captainId;
            this.collection = new App.Players.PlayerCollection();
            this.collection.on('add', this.setCaptainName, this);
            this.collection.fetch();
        },

        render: function () {
            this.$el.append(this.template(this.model.toJSON()));

            return this;
        },
        
        setCaptainName: function (player) {
            if (player.attributes.id === this.captainId) {
                var playerName = player.attributes.firstName + ' ' + player.attributes.lastName;
                $('.captainName').html(playerName);
            }
            
           if (this.model.attributes.id === player.attributes.teamId) {
               var view = new App.Players.PlayerView({model: player});
               this.$('.playersOfThisTeam').append(view.render().$el);
           }
        },

        cancel: function () {
            vm.mediator.publish('ShowTeams');
        },

        edit: function () {
            this.remove();

            vm.mediator.publish('EditTeam', this.model);
        },

        confirmDelete: function () {
            vm.mediator.publish('Popup', 'Вы действительно хотите удалить профиль команды?', this.delete.bind(this));
        },

        delete: function () {
            this.model.destroy();

            vm.mediator.publish('ShowTeams');

            vm.mediator.publish('Notice', 'success', ' успешно удален!');
        }
    });
})(App.Teams);