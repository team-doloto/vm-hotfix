'use strict';

(function (This) {
    This.TeamCollectionView = Backbone.View.extend({
        tagName: 'div',
        className: 'teamCollection',

        template: teamCollectionTpl,

        events: {
            'click button.create': 'create'
        },

        initialize: function () {
            this.collection = new This.TeamCollection();

            this.collection.comparator = function (team) {
                return team.get('name');
            };

            vm.mediator.subscribe('TeamSaved', this.saveModel, {}, this);
            vm.mediator.subscribe('ShowNavMenu:Team', this.render, {}, this);
            vm.mediator.subscribe('Team', this.addOne, {}, this);

            this.listenToOnce(this.collection, 'sync', this.update);

            this.collection.fetch();
        },

        saveModel: function (model) {
            this.collection.add(model);
			this.update();
        },

        update: function () {
            this.render();

            vm.mediator.publish('addNavigator', {collection: this.collection, channel: 'Team'}, null, this);
        },

        render: function (tpl) {
            this.collection.sort();
            this.$el.html(this.template());
            if (tpl) {this.$el.append(tpl)}

            return this;
        },

        addOne: function (team) {
            var view = new This.TeamView({model: team});

            this.$('.list-group').append(view.render().el);
        },

        create: function () {
            vm.mediator.publish('CreateTeam');
        },

        show: function () {
            this.$el.removeClass('hidden');
            vm.mediator.publish('addNavigator', {collection: this.collection, channel: 'Team'}, null, this);
        },

        hide: function () {
            this.$el.addClass('hidden');
        },

        getModelById: function (id, callback) {
            if (this.collection.get(id)) {
                callback(this.collection.get(id));
            } else {
                this.collection.once('sync', function () {
                    if (this.collection.get(id)) {
                        callback(this.collection.get(id));
                    } else {
                        vm.mediator.publish('Show404');
                    }
                }, this);
            }
        }
    });
})(App.Teams);