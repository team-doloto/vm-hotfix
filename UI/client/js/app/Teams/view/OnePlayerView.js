(function (This) {
    This.OnePlayerView = Backbone.View.extend({
        tagName: 'option',

        events: {
            'click': 'mediatorPublish'    
        },

        mediatorPublish: function () {
            vm.mediator.publish('captainSelected', this.model);
        },

        render: function () {
            this.$el.val(this.model.get('id'));
            this.$el.html(this.model.get('firstName') + ' ' + this.model.get('lastName'));
            return this;
        }
    });
})(App.Teams);