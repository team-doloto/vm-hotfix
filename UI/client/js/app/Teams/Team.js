'use strict';

(function (This) {
    This.Team = Backbone.Model.extend({
        urlRoot: '/OData/Teams',

        defaults: function () {
            return {
                'name':'',
                'captainId':'',
                'coach':'',
                'achievements':''
            };
        }, 

        validation: {
            name: [
                {
                    required: true,
                    msg: 'Поле не может быть пустым'
                }, {
                    maxLength: 30,
                    msg: 'Поле не может содержать более 30 символов'
                }
            ],
            achievements: [
                {
                    maxLength: 4000,
                    msg: 'Поле не может содержать более 4000 символов'
                }
            ]
        }
    });
})(App.Teams);