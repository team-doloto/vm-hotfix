'use strict';

(function (This)  {
    This.Router = Backbone.Router.extend({
        routes: {
            'Teams': 'getTeams',
            'Teams/new': 'createTeam',
            'Teams/:id': 'getTeam',
            'Teams/:id/edit': 'editTeam',
            'Teams*path': 'notFound',
        },
        
        initialize: function () {
            this.controller = new App.Teams.Controller();

            //URL navigation
            vm.mediator.subscribe('ShowTeams', this.navigateTeams, null, this);
            vm.mediator.subscribe('ShowTeamById', this.navigateShowTeamById, null, this);
            vm.mediator.subscribe('EditTeam', this.navigateEditTeam, null, this);
            vm.mediator.subscribe('CreateTeam', this.navigateCreateTeam, null, this);
            vm.mediator.subscribe('EditTeamById', this.navigateEditTeamById, null, this);
            vm.mediator.subscribe('ShowTeamInfo', this.navigateShowTeam, null, this);

            
            Backbone.history.loadUrl(Backbone.history.fragment);
        },

        navigateTeams: function () {
            this.navigate('Teams');
        },

        navigateShowTeamById: function (id) {
            this.navigate('Teams/' + id);
        },

        navigateEditTeam: function (team) {
            this.navigate('Teams/' + team.id + '/edit');
        },

        navigateCreateTeam: function () {
            this.navigate('Teams/new');
        },

        navigateEditTeamById: function (team) { 
            this.navigate('Teams/' + team + '/edit');
        },

        navigateShowTeam: function (team) {
            this.navigate('Teams/' + team.id);
        },

        getTeams: function () {
            vm.mediator.publish('ShowTeams');
        },

        getTeam: function (id) {
            vm.mediator.publish('ShowTeamById', id);
        },

        editTeam: function (id) {
            vm.mediator.publish('EditTeamById', id);
        },

        createTeam: function () {
            vm.mediator.publish('CreateTeam');
        },

        notFound: function () {
            vm.mediator.publish('Show404View');
        }
    });
})(App.Teams);