'use strict';

(function (This) {
    This.MenuItemView = Backbone.View.extend({
        tagName: 'li',
        className: 'menu-item',

        template: menuItemTpl,

        events: {
            'click': 'changePath'
        },

        render: function (model) {
            this.model = model;

            this.$el.html(this.template({item: this.model.get('name')}));

            return this;
        },

        changePath: function () {
            vm.mediator.publish('Pressed menu item', this.model.get('path'));
		    $('.menu-item').removeClass('active');
            this.$el.addClass('active');
        }
    });
})(App.Menu);