"use strict";

(function (This) {
    This.MenuItemsCollection = Backbone.Collection.extend({
        url: '/OData/Menu',

        model: This.MenuItem
    });
})(App.Menu);