'use strict';

(function (This) {
    This.Controller = function () {
        var players = new This.PlayerCollectionView(),
            $players = $('#main'),
            view;

        start();
        
        function start () {
            setUpMediator();
            $players.append(players.render().el);
        }

        function setUpMediator () {
            vm.mediator.subscribe('CreatePlayer', createView);
            
            vm.mediator.subscribe('EditPlayer', editView);
            vm.mediator.subscribe('EditPlayerById', editViewById);
            vm.mediator.subscribe('DeletePlayerById', deleteViewById);
            
            vm.mediator.subscribe('ShowPlayerInfo', showView);
            vm.mediator.subscribe('ShowPlayers', showAll);
            vm.mediator.subscribe('ShowPlayerById', showViewById);

            vm.mediator.subscribe('PlayerViewClosed', viewClosed);
        }

        function showAll () {
            hideAll();
            view && view.remove();

            players.show();
        }

        function createView () {
            hideAll();
            view && view.remove();
            view = new This.CreateEditView();

            players.hide();
            $players.append(view.render().el);
        }

        function editView (player) {
            hideAll();
            view && view.remove();
            view = new This.CreateEditView({model: player});
            players.hide();
            $players.append(view.render().el);
        }

        function showView (player) {
            hideAll();
            view && view.remove();
            view = new This.PlayerHomepageView({model: player});
            players.hide();
            $players.append(view.render().el);
        }

        function deleteViewById(id) {
            players.getModelById(id, deleteView)
        }

        function deleteView (player) {
            view && view.remove();
            view = new This.PlayerHomepageView({model: player});
            view.confirmDelete();
        }

        function editViewById (id) {
            players.getModelById(id, editView);
        }

        function viewClosed ( id) {
            if(!id) {
                showAll();
            }else{
                players.getModelById(id, showView);
            }
        }

        function showViewById (id) {
            players.getModelById(id, showView);
         }

        function hideAll () {
            $players.children().addClass('hidden');
        }

        return this;
    }
})(App.Players);