'use strict';

(function (This) {
    This.Player = Backbone.Model.extend({
        urlRoot: '/OData/Players',
        
        defaults: function () {
            return {
                'firstName': '',
                'lastName': '',
                'birthYear': '',
                'teamId': '',
                'height': '',
                'weight': ''
            };
        },
        validation: {
            firstName: [
                {
                    required: true,
                    msg: 'Поле не может быть пустым'
                },
                {
                    minLength: 2,
                    msg: 'Поле не может содержать менее 2 символов'
                }, 
                {
                    maxLength: 60,
                    msg: 'Поле не может содержать более 60 символов'
                }, 
                {
                    pattern: 'lettersOnly',
                    msg: 'Поле должно содержать только буквы'
                }
            ],
            lastName: [
                {
                    required: true,
                    msg: 'Поле не может быть пустым'
                },
                {
                    minLength: 2,
                    msg: 'Поле не может содержать менее 2 символов'
                }, 
                {
                    maxLength: 60,
                    msg: 'Поле не может содержать более 60 символов'
                }, 
                {
                    pattern: 'lettersOnly',
                    msg: 'Поле должно содержать только буквы'
                }
            ],

            birthYear: [
                {
                    required: false,
                    msg: 'Поле должно содержать только цифры'
                },
                {
                    min: 1900,
                    max: 2015,
                    msg: 'Год рождения должен быть в пределах 1900 - 2015' 
                }
            ],
            height: [
                {
                    required: false,
                    msg: 'Поле должно содержать только цифры'
                },
                {
                    min: 100,
                    max: 250,
                    msg: 'Рост должен быть в пределах 100 - 250'
                }
            ],
            weight: [
                {
                    required: false,
                    msg: 'Поле должно содержать только цифры'
                },
                {
                    min: 10,
                    max: 200,
                    msg: 'Вес должен быть в предеах 10 - 200'
                }
            ]
        }
    });
})(App.Players);