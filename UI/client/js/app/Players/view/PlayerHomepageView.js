"use strict";

(function (This) {
    This.PlayerHomepageView = Backbone.View.extend({
        tagName: 'div',
        className: 'homepage',

        template: playerHomepageTpl,

        events: {
            'click .cancel': 'cancel',
            'click .edit': 'edit',
            'click .delete': 'confirmDelete'
        },

        render: function () {
            var teams = new App.Teams.TeamCollection();

            teams.on('sync', function () {
                var player = this.model.toJSON(),
                    playersTeam = teams.get(player.teamId).toJSON();

                 this.$el.append(this.template({player: player, team: playersTeam}));                
            }, this)    

            teams.fetch();

            return this;
        },

        cancel: function () {
            vm.mediator.publish('ShowPlayers');
        },

        edit: function () {
            this.remove();

            vm.mediator.publish('EditPlayer', this.model);
        },

        confirmDelete: function () {
            vm.mediator.publish('Popup', 'Вы действительно хотите удалить профиль этого игрока?', this.delete.bind(this));			
		},

        delete: function () {
            this.model.destroy();

            vm.mediator.publish('ShowPlayers');

            vm.mediator.publish('Notice', 'success', 'Профиль игрока успешно удален!');
        }
    });
})(App.Players);