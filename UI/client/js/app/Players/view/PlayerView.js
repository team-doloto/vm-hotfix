'use strict';

(function (This) {
    This.PlayerView = Backbone.View.extend({
        tagName: 'li',
        className: 'player list-group-item',

        template: playerTpl,

        events: {
            'click': 'showInfo',
            'contextmenu': 'showContextMenu'
        },

        initialize: function () {
            this.model.on('change', this.render, this);
            this.model.on('remove', this.remove, this);
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));

            return this;
        },

        showContextMenu: function (e) {
            var options = {
                    id: this.model.get('id'),
                    clientX: e.clientX,
                    clientY: e.clientY,
                    nameView: 'Player'
                };

            $('#contextMenu').attr('playerId', options.id);

            vm.mediator.publish('Context Menu was called', options);

            e.preventDefault();
        },

        showInfo: function () {
            vm.mediator.publish('ShowPlayerInfo', this.model);
        }
    });
})(App.Players);