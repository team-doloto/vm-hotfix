'use strict';

(function (This) {
    This.UserCollectionView = Backbone.View.extend({
        tagName: 'div',
        className: 'userCollection',

        template: userCollectionTpl,

        events: {
            'click button.create': 'create'
        },

        initialize: function () {
            this.collection = new This.UserCollection();

            vm.mediator.subscribe('UserSaved', this.saveModel, {}, this);
            vm.mediator.subscribe('ShowNavMenu:User', this.render, {}, this);
            vm.mediator.subscribe('User', this.addOne, {}, this);

            this.listenToOnce(this.collection, 'sync', this.update);

            this.collection.fetch();
        },

        saveModel: function (model) {
            this.collection.add(model);
			this.update();
        },

        update: function () {
            this.render();

            vm.mediator.publish('addNavigator', {collection: this.collection, channel: 'User'}, null, this);
        },

        render: function (tpl) {
            this.$el.html(this.template());
            if (tpl) {this.$el.append(tpl)}

            return this;
        },

        addOne: function (user) {
            var view = new This.UserView({model: user});

            this.$('.list-group').append(view.render().el);
        },

        create: function () {
            vm.mediator.publish('CreateUser');
        },

        show: function () {
            vm.mediator.publish('addNavigator', {collection: this.collection, channel: 'User'}, null, this);
            this.$el.removeClass('hidden');
        },

        hide: function () {
            this.$el.addClass('hidden');
        },

        getModelById: function (id, callback) {
            if (this.collection.get(id)) {
                callback(this.collection.get(id));
            } else {
                this.collection.once('sync', function () {
                    if (this.collection.get(id)) {
                        callback(this.collection.get(id));
                    } else {
                        vm.mediator.publish('Show404');
                    }
                }, this);
            }
        }
    });
})(App.Users);