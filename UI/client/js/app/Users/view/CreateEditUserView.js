'use strict';

(function (This) {
    This.CreateEditView = Backbone.View.extend({
        tagName: 'div',

        template: editUserTpl,

        events: {
            'click .save': 'save',
            'click .cancel': 'cancel',
            'blur input': 'preValidate'
        },

        initialize: function () {
            this.model = this.model || new This.User();
            this.defaultModelJSON = this.model.toJSON();
            this.modelBinder = new Backbone.ModelBinder();

            Backbone.Validation.bind(this);

            vm.mediator.subscribe('ShowUserById', this.undoChanges, {}, this);
        },

        render: function () {
            this.$el.append(this.template(this.model.toJSON()));

            this.modelBinder.bind(this.model, this.el);

            return this;
        },

        preValidate: function (e) {
            var attrName, error, validationResult,
                errors = {};
            if (e) {
                attrName = e.target.name;
                error = this.model.preValidate(
                    attrName, this.model.get(attrName)
                );

                if (error) {
                    vm.mediator.publish('Hint', error, this.$('[name=' + attrName + ']'));
                }

                validationResult = error;
            } else {
                errors = this.model.preValidate({
                    name: this.model.get('name'),
                    email: this.model.get('email'),
                    cellPhone: this.model.get('cellPhone'),
                    fullName: this.model.get('fullName'),
                    password: this.model.get('password'),
                    confirmPassword: this.model.get('confirmPassword')
                });

                if (errors) {
                    for (attrName in errors) {
                        vm.mediator.publish('Hint', errors[attrName], this.$('[name=' + attrName + ']'));
                    }
                }
                validationResult = errors;
            }

            return validationResult;
        },

        save: function () {
            var isNewModel = this.model.isNew();

            if (!this.preValidate()) {
                // Don't showing model in CollectionView if model not saved on server
                this.model.once('sync', function () {
                    if (isNewModel) {
                        vm.mediator.publish('UserSaved', this.model);
                    }

                    vm.mediator.publish(
                        'Notice',
                        'success',
                        isNewModel? 'Вы успешно зарегистрировались!': 'Информация успешно изменена!'
                    );

                    vm.mediator.publish(
                        'UserViewClosed',
                        this.model.id
                    );
                }, this);

                this.model.save();


            }
        },

        cancel: function () {
            this.undoChanges();
            vm.mediator.publish(
                'UserViewClosed',
                this.model.id
            );
        },

        undoChanges: function () {
            this.modelBinder.unbind();
            this.model.off('change', this.preValidate);
            this.model.set(this.defaultModelJSON);
        }
    });
})(App.Users);