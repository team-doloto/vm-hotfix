'use strict';

(function (This)  {
    This.Router = Backbone.Router.extend({
        routes: {
            'Users': 'getUsers',
            'Users/new': 'createUser',
            'Users/:id/edit': 'editUser',
            'Users/:id': 'getUser',
            'Users*path': 'notFound'
        },
        
        initialize: function () {
            this.controller = new App.Users.Controller();
            //URL navigation
            vm.mediator.subscribe('CreateUser', this.navigateNewUser, null, this);
            vm.mediator.subscribe('ShowUsers', this.navigateShowUsers, null, this);
            vm.mediator.subscribe('EditUser', this.navigateEditUser, null, this);
            vm.mediator.subscribe('EditUserById', this.navigateEditUserById, null, this);
            vm.mediator.subscribe('ShowUserInfo', this.navigateShowUserInfo, null, this);
            vm.mediator.subscribe('ShowUserById', this.navigateShowUserById, null, this);
            vm.mediator.subscribe('UserViewClosed', this.navigateUserViewClosed, null, this);

            Backbone.history.loadUrl(Backbone.history.fragment);
        },

        navigateNewUser: function () {
            this.navigate('Users/new');
        },

        navigateShowUsers: function () {
            this.navigate('Users');
        },

        navigateEditUser: function (user) {
            this.navigate('Users/' + user.id + '/edit');
        },

        navigateEditUserById: function (userId) {
            this.navigate('Users/' + userId + '/edit');
        },

        navigateShowUserInfo: function (user) {
            this.navigate('Users/' + user.id);
        },
       
        navigateShowUserById: function (userId) {
            this.navigate('Users/' + userId);
        },

        navigateUserViewClosed: function (reason, id) {
            if (reason === 'afterCreating') {
                this.navigate('Users');
            } else {
                this.navigate('Users/' + id);
            };
        },

        getUsers: function () {
            vm.mediator.publish('ShowUsers');
        },

        getUser: function (id) {
            vm.mediator.publish('ShowUserById', id);
        },

        createUser: function () {
            vm.mediator.publish('CreateUser');
        },

        editUser: function (id) {
            vm.mediator.publish('EditUserById', id);
        },

        notFound: function () {
            vm.mediator.publish('Show404View');
        }
    });
})(App.Users);