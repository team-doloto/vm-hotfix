'use strict';

(function (This) {
    This.Controller = function () {
        var users = new This.UserCollectionView(),
            $users = $('#main'),
            view;

        start();
        
        function start () {
            setUpMediator();
            $users.append(users.render().el);
        }

        function setUpMediator () {
            vm.mediator.subscribe('CreateUser', createView);
            
            vm.mediator.subscribe('EditUser', editView);
            vm.mediator.subscribe('EditUserById', editViewById);
            
            vm.mediator.subscribe('ShowUserInfo', showView);
            vm.mediator.subscribe('ShowUsers', showAll);
            vm.mediator.subscribe('ShowUserById', showViewById);
            vm.mediator.subscribe('DeleteUserById', deleteViewById);

            vm.mediator.subscribe('UserViewClosed', viewClosed);
        }

        function showAll () {
            hideAll();
            view && view.remove();

            users.show();
        }

        function createView () {
            hideAll();
            view && view.remove();
            view = new This.CreateEditView();
                        
            users.hide();
            $users.append(view.render().el);
        }

        function editView (user) {
            hideAll();
            view && view.remove();
            view = new This.CreateEditView({model: user});    
            users.hide();
            $users.append(view.render().el);
        }

        function showView (user) {
            hideAll();
            view && view.remove();
            view = new This.UserHomepageView({model: user});    
            users.hide();
            $users.append(view.render().el);
        }

        function editViewById (id) {
            users.getModelById(id, editView);
        }

        function deleteViewById (id) {
            users.getModelById(id, deleteView)
        }

        function deleteView (user) {
            view && view.remove();
            view = new This.UserHomepageView({model: user});
            view.confirmDelete();
        }

        function viewClosed (id) {
            if(!id) {
                showAll();
            }else{
                users.getModelById(id, showView);
            }
        }

        function showViewById (id) {
            users.getModelById(id, showView);
         }

        function hideAll () {
            $users.children().addClass('hidden');
        }

        return this;
    }
})(App.Users);