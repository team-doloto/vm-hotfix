﻿var tournamentTpl = _.template([
    '<div class="row">',
        '<div class="col-sm-9">',
            '<h4><%= name %></h4>',
        '</div>',
        '<div class="col-sm-3">',
            '<h3 class="text-right"><span class="label label-info"><%= season %></span></h3>',
        '</div>',
    '</div>'
].join(''));