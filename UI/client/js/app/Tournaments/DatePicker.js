'use strict';
(function (This)  {
    This.DatePicker = Backbone.Model.extend({
    	defaults: function () { 
    	 	var defaultDate = new Date();
	  	  		
    		return {
    			season: defaultDate.getFullYear(),
				applyingPeriodStart: formatDate(defaultDate),          
				applyingPeriodEnd: formatDate(setTimeline(defaultDate).oneMonthPeriod),						
				gamesStart: formatDate(setTimeline(defaultDate).oneMonthPeriod),
				gamesEnd: formatDate(setTimeline(defaultDate).fourMonthPeriod),
				transferStart: formatDate(setTimeline(defaultDate).twoMonthPeriod),
				transferEnd: formatDate(setTimeline(defaultDate).treeMonthPeriod)
            };
    	}
	});
})(App.Tournaments);











