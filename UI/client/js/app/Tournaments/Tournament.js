'use strict';
(function (This)  {
    This.Tournament = Backbone.Model.extend({
        urlRoot: '/OData/Tournaments',

        defaults: function () {
            return {
                name: '',
                description: '',
                scheme: '1',
                regulationsLink: ''
            };
        },
        validation: {       
            name: [{
                required: true,
                msg: 'Поле не может быть пустым'
            },{
                minLength: 2,
                msg: 'Поле не может содержать менее 2 символов'
            }, {
                maxLength: 60,
                msg: 'Поле не может содержать более 60 символов'
            }],
            description: {
                maxLength: 300,
                msg: 'Поле не может содержать более 300 символов'
            },
            scheme: {
                required: true,
                msg: 'Поле не может быть пустым'
            },
            regulationsLink: {
                pattern: 'url',
                msg: 'Недействительный адрес. Пример:http://www.example.com'
            },       
            season: {
                required: true,
                msg: 'Поле не может быть пустым'
            },          
            applyingPeriodEnd: [{  
                required: true,
                msg: 'Поле не может быть пустым'
            },
            {
              pattern: 'digits',
              msg: 'Только цифры'
            }],
            gamesEnd: [{
                required: true,
                msg: 'Поле не может быть пустым'
            },
            {
              pattern: 'digits',
              msg: 'Только цифры'
            }],
            transferEnd: [{
                required: true,
                msg: 'Поле не может быть пустым'
            },
            {
              pattern: 'digits',
              msg: 'Только цифры'
            }],  
            applyingPeriodStart:
              function (value) { 
                  var msg = '',
                      gameStart = toDate(this.get('gamesStart')),
                      applyingStart = toDate(this.get('applyingPeriodStart')),
                      applyingEnd = toDate(this.get('applyingPeriodEnd'));     
                  if (value !== '') {                 
                      if (applyingEnd < applyingStart) {
                              msg = 'Начало регистрации должно быть раньше конца.';
                              return msg;
                      }
                      if (applyingEnd < setTimeline(applyingStart).oneMonthPeriod) {
                              msg = 'Период регистрации 1 месяц.';
                              return msg;
                      }
                      if (applyingStart === gameStart) {
                              msg = 'Начало регистрации и начало турнира совпадают';
                              return msg;
                      } 
                  } else if (value === '') {
                      msg = 'Поле не может быть пустым';
                      return msg;
                  }
                },   
            gamesStart: 
              function (value) { 
                  var msg = '',
                      gameEnd = toDate(this.get('gamesEnd')),
                      gameStart = toDate(this.get('gamesStart')),
                      applyingStart = toDate(this.get('applyingPeriodStart')),
                      applyingEnd = toDate(this.get('applyingPeriodEnd'));  
                  if (value !== '') {
                      if (gameEnd < gameStart) {
                              msg = 'Начало турнира должно быть раньше конца турнира.';
                              return msg;
                      }
                      if (applyingStart === gameStart) {
                              msg = 'Начало регистрации и начало турнира совпадают';
                              return msg;
                      }
                      if (gameEnd < setTimeline(gameStart).treeMonthPeriod) {
                              msg = 'Период проведения игр 3 месяца.';
                              return msg;
                      }
                      if (applyingStart > gameStart) {
                              msg = 'Начало турнира раньше регистрации';
                              return msg;
                      } 
                      if (applyingEnd > gameStart) {
                              msg = 'Начало турнира раньше конца регистрации';
                              return msg;
                      } 
                  } else if (value === '') {
                      msg = 'Поле не может быть пустым';
                      return msg;
                  }
                },   
            transferStart: 
               function (value) { 
                  var msg = '',
                      gameStart = toDate(this.get('gamesStart')),
                      transferFinish = toDate(this.get('transferEnd')),
                      transferBegin = toDate(this.get('transferStart'));
                  if (value !== '') {
                      if (transferFinish < transferBegin) {
                              msg = 'Начало трансферного окна должно быть раньше конца';
                              return msg;
                      }
                      if (transferFinish < setTimeline(transferBegin).oneMonthPeriod) {
                              msg = 'Период трансферного окна 1 месяц';
                              return msg;
                      }
                      if (transferBegin < setTimeline(gameStart).oneMonthPeriod) {
                              msg = 'Начало трансферного окна через 1 месяца после начала игр';
                              return msg;
                      } 
                  } else if (value === '') {
                      msg = 'Поле не может быть пустым';
                      return msg;
                  }
            },
        }
    });
})(App.Tournaments);

