'use strict';

(function (This) {
    This.Controller = function () {
        var collection = new This.TournamentCollection(),
            generalPage = new This.GeneralPageView({collection: collection}),
            tournaments = new This.TournamentCollectionView({collection: collection}),
            $tournaments = $('#main'),
            view, datePickersView;
			
		collection.comparator = function(chapter) {
            return chapter.get("season");
        };	
		
        start();

        function start () {
            setUpMediator();
        }

        function setUpMediator () {
            vm.mediator.subscribe('Collection loaded', render);
            vm.mediator.subscribe('GeneralPage loaded', showGeneralPage);

            vm.mediator.subscribe('ShowTournaments', showAll);
            vm.mediator.subscribe('ShowTournamentInfo', showView);
            vm.mediator.subscribe('ShowTournamentById', showViewById);

            vm.mediator.subscribe('CreateTournament', createView);
            vm.mediator.subscribe('EditTournament', editView);
            vm.mediator.subscribe('EditTournamentById', editViewById);
            vm.mediator.subscribe('DeleteTournamentById', deleteViewById);

            vm.mediator.subscribe('TournamentViewClosed', viewClosed);
        }

        function render () {
            hideAll();
            $tournaments.append(generalPage.render().el);
            $tournaments.append((tournaments.render().el));
            tournaments.hide();
        }

        function showGeneralPage () {
            hideAll();
            generalPage.show();
        }

        function showAll () {
            hideAll();
            view && view.remove();

            tournaments.show();
        }

        function createView () {
            hideAll();
            view && view.remove();
            view = new This.CreateEditView();

            tournaments.hide();
            $tournaments.append(view.render().el);

			createDatePickersView(view.model);
        }

        function editView (tournament) {
            hideAll();
            view && view.remove();
            view = new This.CreateEditView({model: tournament});

            tournaments.hide();
            $tournaments.append(view.render().el);
		    
			createDatePickersView(tournament);
        }
		
        function createDatePickersView (tournament) {
            datePickersView && datePickersView.remove();
            
			datePickersView = new This.TournamentDatePickersView({model: tournament});
			$('.tournamentDatePickers').html("");
            $('.tournamentDatePickers').append(datePickersView.render().el);
			datePickersView.setHendlers();
        }

        function showView (tournament) {
            hideAll();
            view && view.remove();
            view = new This.TournamentHomepageView({model: tournament});

            tournaments.hide();
            $tournaments.append(view.render().el);
        }

        function editViewById (id) {
            tournaments.getModelById(id, editView);
        }

        function viewClosed (id) {
            if(!id) {
                showAll();
            }else{
                tournaments.getModelById(id, showView);
            }
        }

        function showViewById (id) {
            tournaments.getModelById(id, showView);
        }

        function deleteViewById (id) {
            tournaments.getModelById(id, deleteView);
        }

        function deleteView (tournament) {
            view && view.remove();
            view = new This.TournamentHomepageView({model: tournament});
            view.confirmDelete();
        }

        function hideAll () {
            $tournaments.children().addClass('hidden');
        }

        

        return this;
    }
})(App.Tournaments);