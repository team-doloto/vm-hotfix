"use strict";
(function (This) {
    This.TournamentDatePickersView = Backbone.View.extend({
        template: TournamentDatePickersTpl,
		dp: {},

		events : {
		   "dp.change .leftCalendar": "onChangeLeft",
		   "dp.change .rightCalendar": "onChangeRight"
		},
		
		onChangeLeft: function (e) {		
				this.setLeftDPickerHandler(e, e.target.id);	
		},

		onChangeRight: function (e) {
			    this.setRightDPickerHandler(e.target.id);
		},

		initialize: function () {
		    this.model = this.model || new This.DatePicker(); 
            this.modelBinder = new Backbone.ModelBinder();

            Backbone.Validation.bind(this);    
        },

		
        render: function (itaName) {
        	//var formattedDate = this.formatDate(this.model.toJSON());  // for .NET server

            this.$el.html(this.template(this.model.toJSON()));
            this.modelBinder.bind(this.model, this.$el);
            return this;
        },
		
		setHendlers: function () {
		    var $datePickers = this.$el.find(".datePicker");
			
			_.forEach($datePickers, function(datePicker) {
			    var datePickerName = datePicker.id;

			    this.dp[datePickerName] = datePicker;
				$(this.dp[datePickerName]).datetimepicker(this.setDPickerHash(datePickerName));
			}, this);
		},
        
		setDPickerHash: function (attrName) {
		   return {
				pickTime: false, 
				language: 'ru' 
		   }
		},
		
		setLeftDPickerHandler: function (e, attrName) {
			var val = this.$el.find("input[name='"+attrName+"']").val();	  
			this.model.set(attrName, val);
		},
		
		setRightDPickerHandler: function (attrName) {
			var val = this.$el.find("input[name='"+attrName+"']").val(),
				dateVal = val;
			this.model.set(attrName, dateVal);
		},
		// this function for .NET server
        formatDate: function (attributes) {
            var dateAttributes = [
                'applyingPeriodStart',
                'applyingPeriodEnd',
                'gamesStart',
                'gamesEnd',
                'transferStart',
                'transferEnd'
                ];

            _.each(attributes, function (value, attribute) {
                if (dateAttributes.indexOf(attribute) !== -1) {
                    var date = new Date(value),
                        day = ('0' + date.getDay()).substr(-2),
                        month = ('0' + (date.getMonth() + 1)).substr(-2),
                        year = date.getFullYear();

                    attributes[attribute] = day + '.' + month + '.' + year; 
                }
            });

            return attributes;
        }
    });
})(App.Tournaments);