
'use strict';

(function (This) {
    This.TournamentView = Backbone.View.extend({
        tagName: 'li',
        className: 'tournament list-group-item',

        template: tournamentTpl,

        events: {
            'click': 'showInfo',
            'contextmenu': 'showContextMenu'
        },

        initialize: function () {
            this.model.on('change', this.render, this);
            this.model.on('remove', this.remove, this);
        },

        render: function () {		  
            this.$el.html(this.template(this.model.toJSON())); 
            return this;
        },

        showInfo: function () {
            vm.mediator.publish('ShowTournamentInfo', this.model);
        },

        showContextMenu: function (e) {
            var options = {
                    id: this.model.get('id'),
                    clientX: e.clientX,
                    clientY: e.clientY,
                    nameView: 'Tournament'
                };

            $('#contextMenu').attr('tournamentId', options.id);
            vm.mediator.publish('Context Menu was called', options);
            e.preventDefault();
        }
    });
})(App.Tournaments);