'use strict';

(function (This) {
    This.CreateEditView = Backbone.View.extend({
        tagName: 'div',
        template: editTournamentTpl,

        events: {
            'click .save': 'save',
            'click .cancel': 'cancel',
            'blur  input': 'preValidate'
        },

        initialize: function () {
            var isNewModel;
            this.model = this.model || new This.Tournament();
            this.defaultModelJSON = this.model.toJSON();
            
            isNewModel = this.model.isNew();
            if (isNewModel) {
                this.model.set(new This.DatePicker().toJSON());
            }
            this.modelBinder = new Backbone.ModelBinder();
            Backbone.Validation.bind(this);
            vm.mediator.subscribe('ShowTournamentById', this.undoChanges, {}, this);
        },

        render: function () {
            this.$el.append(this.template(this.model.toJSON()));
            this.modelBinder.bind(this.model, this.el);
            return this;
        },

        preValidate: function (e) {
            var attrName, error, validationResult,
                errors = {};
            if (e) {
                attrName = e.target.name;
                error = this.model.preValidate(attrName, this.model.get(attrName));
                if (error) {
                    vm.mediator.publish('Hint', error, this.$('[name=' + attrName + ']'));
                }

            validationResult = error;
            } else {
                errors = this.model.preValidate({
                    name: this.model.get('name'),
                    description: this.model.get('description'),
                    scheme: this.model.get('scheme'),
                    regulationsLink: this.model.get('regulationsLink'),
                    season: this.model.get('season'),
                    applyingPeriodStart: this.model.get('applyingPeriodStart'),
                    applyingPeriodEnd: this.model.get('applyingPeriodEnd'),
                    gamesStart: this.model.get('gamesStart'),
                    gamesEnd: this.model.get('gamesEnd'),
                    transferStart: this.model.get('transferStart'),
                    transferEnd: this.model.get('transferEnd')
                });
                if (errors) {
                    for (attrName in errors) {
                        vm.mediator.publish('Hint', errors[attrName], this.$('[name=' + attrName + ']'));
                    }
                }
                validationResult = errors;
            }

            return validationResult;        
        },

        save: function () {
            var isNewModel = this.model.isNew();
            if (!this.preValidate()) {
                this.model.once('sync', function () {
                    if (isNewModel) {
                        vm.mediator.publish('TournamentSaved', this.model);
                    }
                    vm.mediator.publish(
                        'Notice',
                        'success',
                        isNewModel? 'Турнир успешно создан': 'Турнир успешно изменён'
                    );

                    vm.mediator.publish(
                        'TournamentViewClosed',
                        this.model.id
                    );
                }, this);
                //this.convertToDate(); //for .NET server
                this.model.save();               

            } 
        },

        cancel: function () {
            this.undoChanges();

            vm.mediator.publish(
                'TournamentViewClosed',
                this.model.id
            );
        },

        undoChanges: function () {
            this.modelBinder.unbind();
            this.model.off('change', this.preValidate);
            this.model.set(this.defaultModelJSON);
        },
        // this function for .NET server
        convertToDate: function () {
            var dateAttributes = [
                'applyingPeriodStart',
                'applyingPeriodEnd',
                'gamesStart',
                'gamesEnd',
                'transferStart',
                'transferEnd'
                ],
                newDateAttributes = {};

            _.each(this.model.attributes, function (value, attribute) {
                if (dateAttributes.indexOf(attribute) !== -1) {
                    var arrItemDate = value.split('.'),
                        day = Number(arrItemDate[0]),
                        month = Number(arrItemDate[1]) - 1,
                        year = Number(arrItemDate[2]);

                    newDateAttributes[attribute] = (new Date(year, month, day)).toJSON(); 
                }
            });

            this.model.set(newDateAttributes);
        }
    });
})(App.Tournaments);