'use strict';

(function (This) {
    This.NavigatorView = Backbone.View.extend({
        tagName: 'div',
        className: '',

        template: navigatorTpl,

        navIndex: (sessionStorage.nav)? JSON.parse(sessionStorage.nav): {},
        sizeList: [5, 10, 20, 50],

        events: {
            'click .return': 'render',
            'click .dropdown-element': 'setPageSize',
            'keyup .searchField': 'startSearch',
            'click .pageEl': 'addOne',
            'click .showSearch': 'showSearch'
        },

        initialize: function (input) {
            this.collection = input.collection;
            this.channel = input.channel;
            this.pageSize = input.pageSize || 10;
            this.render();
        },

        render: function () {
            var pageCount = Math.ceil(this.collection.length / this.pageSize),
                startPosition,
                finishPosition,
                i;

            this.$el.html(this.template({
                pageSize: this.pageSize,
                sizeList: this.sizeList,
                pageCount: pageCount
            }));

            vm.mediator.publish('ShowNavMenu:' + this.channel, this.$el, null, this);

            this.index = this.navIndex[this.channel] || 0;

            startPosition = this.index * this.pageSize;
            finishPosition = startPosition + this.pageSize;

            //figure out if our collection has model with № startPosition
            if(!this.collection.models[startPosition]){
                startPosition = startPosition - this.pageSize;
            }

            for (i = startPosition; i < finishPosition; i++) {

                if (this.collection.models[i]) {
                    vm.mediator.publish(this.channel, this.collection.models[i], null, this);
                }
            }

            this.$(".pagination li").eq(this.index).addClass('active');
            this.delegateEvents();
            this.$('.searchField').focus();
            return this;
        },

        setPageSize: function (e) {
            this.pageSize = e.currentTarget.value;
            this.index = 0;
            this.render();
        },


        addOne: function (e) {
            this.navIndex[this.channel] = e.currentTarget.value - 1;
            this.render();
        },

        showSearch: function () {
            this.$('.search-group').toggleClass('hidden');
        },

        startSearch: function () {
            var searchRequest = this.$('.searchField').val();

            if (searchRequest !== '') {
                this.filter(searchRequest);
            } else {
                this.render()
            }
            this.delegateEvents();
            this.$('.search-group').removeClass('hidden');
            this.$('.searchField').focus();
        },


        filter: function (value) {
            var filters = {
                    'User': ['name'],
                    'Tournament': ['name'],
                    'Player': ['lastName', 'firstName'],
                    'Team': ['name']
                },
                param = filters[this.channel],
                filteredCollection;
                
            filteredCollection = this.collection.filter(function (model) {
                var isAppropriate = false,
                    attributes;
                    
                param.forEach(function (paramName) {
                    attributes = attributes + ' ' + model.get(paramName);    
                });          

                if(attributes.toLowerCase().indexOf(value.toLowerCase()) >= 0) {
                    isAppropriate = true;
                };
                return isAppropriate;

            });

            this.$el.html(this.template({pageSize: this.pageSize, sizeList: this.sizeList, pageCount: 0}));
            vm.mediator.publish('ShowNavMenu:' + this.channel, this.$el, null, this);

            _.each(filteredCollection, function (filteredModel) {
                vm.mediator.publish(this.channel, filteredModel, null, this);
            }, this);

            vm.mediator.publish('Notice', 'success', 'Найдено ' + filteredCollection.length + ' результатов');
            this.$('.searchField').val(value);
        },

        removeMe: function () {
            sessionStorage.nav = JSON.stringify(this.navIndex);
            this.remove();
        }

    });
})(App.Navigator);