'use strict';

(function (This)  {
    This.Router = Backbone.Router.extend({
        routes: {
            '': 'index',
            'Home': 'index',
            'Tournaments*path': 'tournaments',
            'Users*path': 'users',
            'Players*path': 'players',
            'About*path': 'about',
            'Teams*path': 'teams',
            '*path': 'notFound'
        },
		
        initialize: function () {
            //URl navigation
            vm.mediator.subscribe('MainPageSelected', this.navigateMainPage, null, this);
            vm.mediator.subscribe('MenuItemSelected', this.navigateMenuItem, null, this);
        },

        navigateMainPage: function () {
            this.navigate('/', {trigger: true});
        },

        navigateMenuItem: function (pathname) {
            this.navigate(pathname, {trigger: true});
        },

        index: function () {
            vm.subRouters['Tournaments'] || (vm.subRouters['Tournaments'] = new App.Tournaments.Router({root: '/WebApi'}));
        },

        tournaments: function () {
            vm.subRouters['Tournaments'] || (vm.subRouters['Tournaments'] = new App.Tournaments.Router({root: '/WebApi'}));
        },

        users: function () {
            vm.subRouters['Users'] || (vm.subRouters['Users'] = new App.Users.Router({root: '/WebApi'}));
        },

        players: function () {
            vm.subRouters['Players'] || (vm.subRouters['Players'] = new App.Players.Router({root: '/WebApi'}));
        },

        about: function () {
            vm.subRouters['About'] || (vm.subRouters['About'] = new App.About.Router({root: '/WebApi'}));
        },

        teams: function () {
            vm.subRouters['Teams'] || (vm.subRouters['Teams'] = new App.Teams.Router({root: '/WebApi'}));
        },

        notFound: function () {
            vm.mediator.publish('Show404View');
        }
    });
})(App);