'use strict';
function setUp (parent, modules) {
    modules.forEach(function (module) {
        parent[module] = {};
    });
}

function toDate (strDate) {
    var dateParts = strDate.split('.');
        return new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
}

function formatValue (value) {
    return (value < 10)? ('0' + value): value;
}

function setTimeline (defaultDate) {
    var intervals = {
        currentPeriod: 1,
        registrationPeriod: 2,
        transferPeriod: 3,
        tournamentDuration: 4
        };
       
    return {
        oneMonthPeriod:  toDateObj(defaultDate, intervals.currentPeriod),
        twoMonthPeriod: toDateObj(defaultDate, intervals.registrationPeriod),
        treeMonthPeriod: toDateObj(defaultDate, intervals.transferPeriod),
        fourMonthPeriod: toDateObj(defaultDate, intervals.tournamentDuration)
    }
}

function toDateObj (defaultDate, interval) {
    return new Date(defaultDate.getFullYear(), defaultDate.getMonth() + interval,
         defaultDate.getDate()) 
}

function formatDate (defaultDate) {
    var day, month, year;
      day = defaultDate.getDate();
      month =   defaultDate.getMonth() + 1;          
      year = defaultDate.getFullYear();

    return formatValue(day) + '.' + formatValue(month) + '.' + formatValue(year);  
}