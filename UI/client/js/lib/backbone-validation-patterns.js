_.extend(Backbone.Validation.patterns, {
  lettersOnly: /^[а-яА-Яa-zA-Z][а-яА-Яa-zA-Z]+$/, 
  digits: /^[0-9.,]+$/
});