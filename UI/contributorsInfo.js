exports.getContributorsInfoList = getContributorsInfoList;

var _ = require("underscore"),
    contrib,
    contributorsInfo =[
        {
             'id':'0',
             'firstName':'Олег',
             'lastName':'Маньков',
             'contributorTeamId': '0'
        },
        {
             'id':'1',
             'firstName':'Станислав',
             'lastName':'Махницкий',
			 'contributorTeamId': '0'
        },
        {
             'id':'2',
             'firstName':'Екатерина',
			 'lastName':'Николаева',
             'contributorTeamId': '0'
        },
        {
             'id':'3',
             'firstName':'Денис',
			 'lastName':'Черныш',
             'contributorTeamId': '0'
        },
        {
             'id':'4',
             'firstName':'Евгений',
			 'lastName':'Альфоров',
             'contributorTeamId': '0'
        },
        {
             'id':'5',
             'firstName':'Олег',
			 'lastName':'Волкодав',
             'contributorTeamId': '1'
        },
        {
             'id':'6',
             'firstName':'Мария',
			 'lastName':'Кочеткова',
             'contributorTeamId': '1'
        },
        {
             'id':'7',
             'firstName':'Виктория',
			 'lastName':'Рындина',
             'contributorTeamId': '1'
        },
        {
             'id':'8',
             'firstName':'Александр',
			 'lastName':'Маха',
             'contributorTeamId': '1'
        },
        {
             'id':'9',
             'firstName':'Дмитрий',
			 'lastName':'Чернышов',
             'contributorTeamId': '1'
        },
        {
             'id':'10',
             'firstName':'Алексей',
			 'lastName':'Лапин',
             'contributorTeamId': '1'
        },
        {
             'id':'11',
             'firstName':'Сергей',
			 'lastName':'Бондаренко',
             'contributorTeamId': '2'
        },
        {
             'id':'12',
             'firstName':'Дмитрий',
			 'lastName':'Отрашевский',
             'contributorTeamId': '2'
        },
        {
             'id':'13',
             'firstName':'Станислав',
			 'lastName':'Завизион',
             'contributorTeamId': '2'
        },
        {
             'id':'14',
             'firstName':'Руслан',
			 'lastName':'Борисенко',
             'contributorTeamId': '2'
        },
        {
            'id':'15',
            'firstName':'Катерина',
            'lastName':'Овчаренко',
            'contributorTeamId': '3'
        },
        {
            'id':'16',
            'firstName':'Ольга',
			'lastName':'Шафаренко',
            'contributorTeamId': '3'
        },		 ,
        {
            'id':'17',
            'firstName':'Даниил',
            'lastName':'Коростиенко',
            'contributorTeamId': '3'
        },
		{
            'id':'18',
            'firstName':'Алла',
            'lastName':'Приходченко',
            'contributorTeamId': '4'
        },
		{
            'id':'19',
            'firstName':'Дмитрий',
            'lastName':'Маслов',
            'contributorTeamId': '4'
        },
		{
            'id':'20',
            'firstName':'Александр',
            'lastName':'Зайцев',
            'contributorTeamId': '4'
        },
		{
            'id':'21',
            'firstName':'Артем',
            'lastName':'Поздеев',
            'contributorTeamId': '4'
        },
		{
            'id':'22',
            'firstName':'Андрей',
            'lastName':'Лантух',
            'contributorTeamId': '4'
        },
		{
            'id':'23',
            'firstName':'Артем',
            'lastName':'Пученко',
            'contributorTeamId': '4'
        },
        {
            'id':'24',
            'firstName':'Егор',
            'lastName':'Дыхов',
            'contributorTeamId': '3'
        },
		{
            'id':'25',
            'firstName':'Карина',
            'lastName':'Чегорко',
            'contributorTeamId': '5'
        },
		{
            'id':'26',
            'firstName':'Алена',
            'lastName':'Борисова',
            'contributorTeamId': '5'
        },
		{
            'id':'27',
            'firstName':'Сергей',
            'lastName':'Андроник',
            'contributorTeamId': '5'
        },
		{
            'id':'28',
            'firstName':'Иван',
            'lastName':'Шитиков',
            'contributorTeamId': '5'
        },
		{
            'id':'29',
            'firstName':'Дмитрий',
            'lastName':'Селезень',
            'contributorTeamId': '5'
        },
		{
            'id':'30',
            'firstName':'Максим',
            'lastName':'Белинский',
            'contributorTeamId': '5'
        },
		{
            'id':'31',
            'firstName':'Алексей',
            'lastName':'Лебедянский',
            'contributorTeamId': '5'
        },
		{
            'id':'32',
            'firstName':'Денис',
            'lastName':'Руденко',
            'contributorTeamId': '6'
        },
		{
            'id':'33',
            'firstName':'Никита',
            'lastName':'Курченков',
            'contributorTeamId': '6'
        },
		{
            'id':'34',
            'firstName':'Элизавета',
            'lastName':'Рудакова',
            'contributorTeamId': '6'
        },
		{
            'id':'35',
            'firstName':'Михаил',
            'lastName':'Зажарский',
            'contributorTeamId': '6'
        },
		{
            'id':'36',
            'firstName':'Дмитрий',
            'lastName':'Рындин',
            'contributorTeamId': '6'
        },
		{
            'id':'37',
            'firstName':'Дмитрий',
            'lastName':'Шаповал',
            'contributorTeamId': '6'
        }
    ];

function getContributorsInfoList () {
    return contrib = _.clone(contributorsInfo);
}