exports.getContextMenuItemsList = getContextMenuItemsList;

var _ = require("underscore"),
    contextMenuItems,
    clearContextMenuItemsList = [
        {
            'id': '0',
            'name': 'Показать полную информацию'
        },
        {
            'id': '1',
            'name': 'Редактировать'
        },
        {
            'id': '2',
            'name': 'Удалить'
        },
        {
            'id': '3',
            'name': 'На главную'
        }
    ];

function getContextMenuItemsList () {
    return contextMenuItems = _.clone(clearContextMenuItemsList);
}