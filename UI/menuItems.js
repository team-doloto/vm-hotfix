exports.getMenuItemsList = getMenuItemsList;

var _ = require("underscore"),
    menuItems,
    clearMenuItemsList = [
        {
            'id': '0',
            'path': '',
            'name': 'Главная'
        },
        {
            'id': '1',
            'path': 'Tournaments',
            'name': 'Турниры'
        },
        {
            'id': '2',
            'path': 'Users',
            'name': 'Пользователи'
        },
        {
            'id': '3',
            'path': 'Teams',
            'name': 'Команды'
        },
        {
            'id': '4',
            'path': 'Players',
            'name': 'Игроки'
        },
        {
            'id': '5',
            'path': 'About',
            'name': 'О нас'
        }
    ];

function getMenuItemsList () {
    return menuItems = _.clone(clearMenuItemsList);
}